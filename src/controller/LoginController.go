package controller

import (
	"sample/jwt-gin/src/model"
	"sample/jwt-gin/src/service"

	"github.com/gin-gonic/gin"
)

// Interface
type LoginController interface {
	Login(ctx *gin.Context) string
}

type loginCtrl struct {
	loginService service.LoginService
	jwtService   service.JWTService
}

func LoginHandler(loginService service.LoginService, jwtService service.JWTService) LoginController {
	return &loginCtrl{
		loginService: loginService,
		jwtService:   jwtService,
	}
}

func (controller *loginCtrl) Login(ctx *gin.Context) string {
	var credential model.LoginCredentials
	err := ctx.ShouldBind(&credential)
	if err != nil {
		return "No Data Found"
	}

	isUserAuthenticated := controller.loginService.LoginUser(credential.Email, credential.Password)

	if isUserAuthenticated {
		return controller.jwtService.GenerateToken(credential.Email, true)
	}
	return ""
}
